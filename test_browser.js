var canvas = document.getElementById("renderCanvas"); // Get the canvas element 
var engine = new BABYLON.Engine(canvas, true); // Generate the BABYLON 3D engine






// same code as test3.js
var scene = new BABYLON.Scene(engine);
scene.clearColor = new BABYLON.Color3(0.8, 0.8, 0.8);
const camera = new BABYLON.ArcRotateCamera('Camera', Math.PI / 180 * -30, Math.PI / 180 * 55, 5, new BABYLON.Vector3(0, 0, 0), scene)
var light = new BABYLON.PointLight("light", new BABYLON.Vector3(10, 10, 0), scene);
var light = new BABYLON.PointLight("light", new BABYLON.Vector3(-10, 10, 10), scene);
var box = BABYLON.Mesh.CreateBox("box", 2, scene);

var boxMaterial = new BABYLON.StandardMaterial("material", scene);
var texture = new BABYLON.Texture('https://bbcdn.githack.com/jddaigle/test_headless_gl/raw/master/grass-old.jpg', scene)
boxMaterial.diffuseTexture = texture
box.material = boxMaterial;
console.log(texture.isBlocking)
console.log(texture.isReadyOrNotBlocking())
console.log(texture.isReady())
// end





engine.runRenderLoop(function () { // Register a render loop to repeatedly render the scene
	scene.render();
});

window.addEventListener("resize", function () { // Watch for browser/canvas resize events
	engine.resize();
});

