width  = 400
height = 400
path   = 'out.png'

PNG = require('pngjs').PNG
fs = require('fs')
png = new PNG({ width: width, height: height })


require('jsdom-global')()
var gl = require('gl')(400,400); //headless-gl

var BABYLON = require("babylonjs");
var fs = require("fs");

//var engine = new BABYLON.Engine(mycanvas, true);
var engine = new BABYLON.Engine(gl, true, {disableWebGL2Support: true});
var scene = new BABYLON.Scene(engine);

// Add a camera to the scene and attach it to the canvas

var camera = new BABYLON.ArcRotateCamera("Camera", Math.PI / 2, Math.PI / 2, 2, BABYLON.Vector3.Zero(), scene);
//camera.attachControl(canvas, true);

// Add lights to the scene
var light1 = new BABYLON.HemisphericLight("light1", new BABYLON.Vector3(1, 1, 0), scene);
var light2 = new BABYLON.PointLight("light2", new BABYLON.Vector3(0, 1, -1), scene);

// This is where you create and manipulate meshes
     fs.exists('grass-old.jpg', (exists) => {
        if (exists) {
		   console.log('exists')
		 } else {
		   console.log('doesnt exists')
		 }
	   })
		
var sphere = BABYLON.MeshBuilder.CreateSphere("sphere", {}, scene);
var texture = new BABYLON.Texture('grass-old.jpg', scene)
sphere.diffuseTexture = texture
console.log(texture.isReady())

texture.onLoadObservable.add((texture) => {
	console.log('Loaded Siding')
})

scene.onAfterRenderObservable.add(() => {
	//console.log(texture.isReady())
		
	// create a pixel buffer of the correct size
	pixels = new Uint8Array(4 * width * height)

	// read back in the pixel buffer
	gl.readPixels(0, 0, width, height, gl.RGBA, gl.UNSIGNED_BYTE, pixels)

	// lines are vertically flipped in the FBO / need to unflip them
	var i,j
	for (j=0; j<=height; j++)
		for (i=0; i<=width; i++)
		{
			k = j * width + i
			r = pixels[4*k]
			g = pixels[4*k + 1]
			b = pixels[4*k + 2]
			a = pixels[4*k + 3]

			m = (height - j + 1) * width + i
			png.data[4*m]     = r
			png.data[4*m + 1] = g
			png.data[4*m + 2] = b
			png.data[4*m + 3] = a
		 }

	// Now write the png to disk
	stream = fs.createWriteStream(path)
	png.pack().pipe(stream)
})

scene.render()

/* BABYLON.Texture.WhenAllReady([texture], () => {
	console.log('Texture.WhenAllReady')
	scene.render()
}) */



