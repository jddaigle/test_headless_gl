width = 400
height = 400
require('jsdom-global')()
var gl = require('gl')(width, height); //headless-gl
path = 'out_test3.png'

PNG = require('pngjs').PNG
fs = require('fs')
png = new PNG({
	width: width,
	height: height
})
var BABYLON = require("babylonjs");
var engine = new BABYLON.Engine(gl, true, {
	disableWebGL2Support: true
});


// same code as test_browser.js
var scene = new BABYLON.Scene(engine);
scene.clearColor = new BABYLON.Color3(0.8, 0.8, 0.8);
const camera = new BABYLON.ArcRotateCamera('Camera', Math.PI / 180 * -30, Math.PI / 180 * 55, 5, new BABYLON.Vector3(0, 0, 0), scene)
var light = new BABYLON.PointLight("light", new BABYLON.Vector3(10, 10, 0), scene);
var light = new BABYLON.PointLight("light", new BABYLON.Vector3(-10, 10, 10), scene);
var box = BABYLON.Mesh.CreateBox("box", 2, scene);

var boxMaterial = new BABYLON.StandardMaterial("material", scene);
var texture = new BABYLON.Texture('https://bbcdn.githack.com/jddaigle/test_headless_gl/raw/master/grass-old.jpg', scene)
boxMaterial.diffuseTexture = texture
box.material = boxMaterial;
console.log(texture.isBlocking)
console.log(texture.isReadyOrNotBlocking())
console.log(texture.isReady())
// end



scene.onAfterRenderObservable.add(() => {
  // create a pixel buffer of the correct size
  pixels = new Uint8Array(4 * width * height)

  // read back in the pixel buffer
  gl.readPixels(0, 0, width, height, gl.RGBA, gl.UNSIGNED_BYTE, pixels)

  // lines are vertically flipped in the FBO / need to unflip them
  var i, j
  for (j = 0; j <= height; j++) {
    for (i = 0; i <= width; i++) {
      k = j * width + i
      r = pixels[4 * k]
      g = pixels[4 * k + 1]
      b = pixels[4 * k + 2]
      a = pixels[4 * k + 3]

      m = (height - j + 1) * width + i
      png.data[4 * m] = r
      png.data[4 * m + 1] = g
      png.data[4 * m + 2] = b
      png.data[4 * m + 3] = a
    }
  }
  // Now write the png to disk
  stream = fs.createWriteStream(path)
  png.pack().pipe(stream)
})

scene.render();


